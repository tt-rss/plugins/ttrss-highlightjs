<?php
class HighlightJS extends Plugin {
	function about() {
		return array(null,
			"Hightlights code blocks in articles using HighlightJS",
			"fox");
	}

	function init($host) {

	}

	function get_css() {
		return file_get_contents(__DIR__ . "/hljs-theme.css");
	}

	function get_js() {
		return file_get_contents(__DIR__ . "/init.js");
	}

	function api_version() {
		return 2;
	}
}
